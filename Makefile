IMAGES = $(patsubst %.vmdb,%.img,$(wildcard *.vmdb))
XZ_IMAGES = $(patsubst %,%.xz,$(IMAGES))
FILES = eth0.network ava-flash

vmdb2 := vmdb2

all: $(IMAGES)

-include conf.mk

$(IMAGES): %.img: %.vmdb $(FILES)
	sudo auto-apt-proxy $(vmdb2) \
		--verbose \
		--log=/dev/stderr \
		--output=$@ \
		--rootfs-tarball=$*.tar \
    $< || ($(RM) $@; false)

xz-images: $(XZ_IMAGES)

$(XZ_IMAGES): %.xz: %
	xz -T0 --force --keep $<

upload: $(XZ_IMAGES)
	@test -n "$(UPLOAD)"
	rsync -avpz $(XZ_IMAGES) $(UPLOAD)

clean:
	$(RM) $(IMAGES) $(XZ_IMAGES)

fullclean: clean
	$(RM) *.tar
