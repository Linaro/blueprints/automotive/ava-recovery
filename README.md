# ava-recovery

This project builds a minimal Debian image for use as a recovery image for the
AVA platform on LAVA.

## Getting started

Necessary dependency: [vmdb2](https://gitlab.com/larswirzenius/vmdb2) 0.26-2 or
later (available in Debian 12 bookworm at the time of writing).

Just run `make`.

## License

MIT/Expat. See [LICENSE.md](LICENSE.md) for the full test of the license.
